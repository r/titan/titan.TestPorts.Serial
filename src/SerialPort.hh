/******************************************************************************
 * Copyright (c) 2000-2019 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Lenard Nagy
 * version R1A
 ******************************************************************************/

#ifndef SerialPort_HH
#define SerialPort_HH

#include "SerialPortTypes.hh"
#include <string.h>
#include <fcntl.h>   /* File control definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <vector>

namespace SerialPortTypes {

  class SerialPort : public SerialPort_BASE {
  public:
    SerialPort(const char *par_port_name = NULL);
    ~SerialPort();

	void set__port__parameter(const CHARSTRING& parameter_name, const CHARSTRING& parameter_value, SerialPort& serial_port);
	
	char* get_device_file_name();
	
    void set_parameter(const char *parameter_name,
		       const char *parameter_value);

  private:
    /* void Handle_Fd_Event(int fd, boolean is_readable,
       boolean is_writable, boolean is_error); */
    void Handle_Fd_Event_Error(int fd);
    void Handle_Fd_Event_Writable(int fd);
    void Handle_Fd_Event_Readable(int fd);
    /* void Handle_Timeout(double time_since_last_call); */
  protected:
    void user_map(const char *system_port);
    void user_unmap(const char *system_port);
    void user_start();
    void user_stop();
    void outgoing_send(const CHARSTRING& send_par);

    int  set_interface_attribs (int fd, speed_t speed, int parity, int dataBits, int stopBit, bool CRtoNL);
    void set_blocking (int fd, int should_block);

    char *param_device_file;
    int device_fd;
    speed_t speed;
    int parity;
    int blocking;
    std::vector<char> eolChars;
    int dataBits;
    int stopBit;
	bool CRtoNL;
    CHARSTRING ret_buffer;
  };

} /* end of namespace */

#endif

